<?php

$output    = '';
$conf_pach = R.'plugins/ulogin/config.json';

$Viewer = new Viewer_Manager;
$config = json_decode(file_get_contents($conf_pach), true);
$template = file_get_contents(dirname(__FILE__).'/template/settings.html');

$arr_default = array(
    "ulogin_id" => "",
    "display" => "small",
    "providers" => "vkontakte,google,mailru,yandex,twitter",
    "hidden" => "other"
);
$config = array_merge($arr_default, $config);

if (isset($_POST['ulogin_id'])) {
    foreach (array_keys($arr_default) as $key) {
        $config[$key] = $_POST[$key];
    }
    file_put_contents($conf_pach, json_encode($config, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
} else {
    $output .= $Viewer->parseTemplate($template, array('config' => $config));
}

?>