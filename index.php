<?php

class ulogin {

    private $DB;

    public function __construct($params) {
        $this->DB = getDB();
        $this->Log = Register::getClass('Logination');
    }


    public function install() {
        $this->DB->query("ALTER TABLE `users` ADD `provider` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;");

        $this->DB->query("CREATE TABLE `users_ulogin` (
            `id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
            `identity` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
            `nickname` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
            PRIMARY KEY ( `id` )
        )");
        return array('status' => 1);
    }


    public function common($params, $hook, $file = false) {
        if ($hook == 'markers_data') {
            return $this->ulogin_widget($params);

        } elseif ($hook == 'before_view' and
                  $file == 'main.html' and
                  isset($_GET['action']) and $_GET['action']=='ulogin' and
                  isset($_POST['token'])
                 ) {
            return $this->ulogin_auth($params);

        } elseif ($hook == 'before_view') {
            return $params;
        }
    }


    // функция добавляет метку {{ ulogin }}, добавляющая виджет авторизации
    public function ulogin_widget($params) {
        // возврат, если метка уже добавлена
        if (isset($params['ulogin'])) return $params;

        $params['ulogin'] = function() use(&$marker) {
            if (!isset($marker)) {
                $config = json_decode(file_get_contents(dirname(__FILE__) . '/config.json'), true);
                $email_activate = Config::read('email_activate');

                $marker = '<script src="//ulogin.ru/js/ulogin.js"></script>';

                // если не указан uLogin ID, то сами выводим виджет, иначе этим займётся uLogin
                if (empty($config['ulogin_id'])) {

                    if ($config['display']=='window') {
                        $marker .= '<a href="#" id="uLogin" data-ulogin="
                                        display=window;
                                        fields='.$config['fields'].';
                                        redirect_uri='.(used_https() ? 'https' : 'http').'%3A%2F%2F'.$_SERVER['HTTP_HOST'].'%3Faction%3Dulogin;">
                                            <img src="//ulogin.ru/img/button.png" width=187 height=30 alt="МультиВход"/>
                                    </a>';
                    } else {
                        $marker .= '<div id="uLogin" data-ulogin="
                                        display='.$config['display'].';
                                        fields='.$config['fields'].';
                                        optional='.$config['optional'].';
                                        providers='.$config['providers'].';
                                        hidden='.$config['hidden'].';
                                        redirect_uri='.(used_https() ? 'https' : 'http').'%3A%2F%2F'.$_SERVER['HTTP_HOST'].'%3Faction%3Dulogin;';
                        $marker .= (!empty($email_activate)) ? 'verify=1;' : '';
                        $marker .= '"></div>';
                    }

                } else {
                    $marker .= '<div id="uLogin_'.$config['ulogin_id'].'" data-uloginid="'.$config['ulogin_id'].'"></div>';
                }

            }
            return $marker;
        };
        return $params;
    }


    // функция производит авторизацию или регистрацию пользователя
    public function ulogin_auth($params) {
        $token = $_POST['token'];

        $s = file_get_contents('http://ulogin.ru/token.php?token='.$token.'&host='.$_SERVER['HTTP_HOST']);
        $user = json_decode($s, true);

        if ($user == NULL)
            return $params;

        if (isset($_POST['nickname'])) {
            $nickname = $_POST['nickname'];
        } else {
            $nickname = $user['nickname'];

            $changname = $this->DB->query("SELECT *
                FROM `users_ulogin`
                WHERE `identity` LIKE '".$user['network'].";".$user['identity']."'");
            if (isset($changname[0])) {
                $nickname = $changname[0]['nickname'];
            }
        }


        if (preg_match("#[^- _0-9a-zА-Яа-я]#ui", $nickname)) {
            $markets['{{ message }}'] = 'Ник <strong>'.$nickname.'</strong> содержит недопустимые символы.
                Пожалуйста, выберите другой ник, под которым Вы будете зарегистрированы на сайте.';
            return $this->error($markets, $nickname, $token);
        }

        if (mb_strlen($nickname) < 3 || mb_strlen($nickname) > 20) {
            $markets['{{ message }}'] = 'Ник <strong>'.$nickname.'</strong> меньше 3 или более 20 символов.
                Пожалуйста, выберите другой ник, под которым Вы будете зарегистрированы на сайте.';
            return $this->error($markets, $nickname, $token);
        }

        // поиск людей с таким ником
        $markets = array();
        $usersModel = \OrmManager::getModelInstance('users');
        $res = $usersModel->getSameNics($nickname);
        if (is_array($res) && count($res) > 0) {
            $prs = explode(';', $res[0]['provider']);
            if (count($prs) > 0 and in_array($user['network'], $prs)) {
                $_SESSION['user'] = $res[0];
                setcookie('autologin', 'yes', time() + 3600 * 24 * Config::read('cookie_time'), '/');
                setcookie('userid', $res[0]['id'], time() + 3600 * 24 * Config::read('cookie_time'), '/');
                setcookie('password', $res[0]['passw'], time() + 3600 * 24 * Config::read('cookie_time'), '/');
                header('Location: '.(used_https() ? 'https://' : 'http://').$_SERVER['HTTP_HOST']);
                return;
            } else {
                $markets['{{ message }}'] = 'Ник <strong>'.$nickname.'</strong> занят.
                    Пожалуйста, выберите другой ник, под которым Вы будете зарегистрированы на сайте.';
                return $this->error($markets, $nickname, $token);
            }
        }

        // ошибок нет, такой юзер отсутствует, регистрируем
        $password = substr(md5(uniqid()),0,6);
        $data = array(
            'name'       => $nickname,
            'passw'      => md5crypt($password),
            'puttime'    => date("Y-m-j H:i:s"),
            'last_visit' => date("Y-m-j H:i:s"),
            'provider'   => $user['network'],
            'email'      => $user['email'],
            'status'     => 1
        );

        if (isset($user['first_name']) and isset($user['last_name']))
            $data['full_name'] = $user['first_name'].' '.$user['last_name'];

        if (isset($user['sex']))
            $data['pol'] = ($user['sex']='1') ? 'm' : 'f';

        if (isset($user['profile']))
            $data['url'] = $user['profile'];

        if (isset($user['bdate'])) {
            $udata = date_parse($user['bdate']);
            if (count($udata['errors']) == 0) {
                $data['bday']   = $udata['day'];
                $data['bmonth'] = $udata['month'];
                $data['byear']  = $udata['year'];
            }
        }

        $data['id'] = $this->DB->save('users', $data);

        if ($data['id'] == 0)
            return $params;

        // запись если ник изменён
        if ($user['nickname'] != $nickname) {
            $this->DB->save('users_ulogin', array(
                'identity'      => $user['network'].";".$user['identity'],
                'nickname'      => $nickname
            ));
        }
        
        $mailer = new \AtmMail();
        $subject = 'Добро пожаловать';
        $message_html = 'Здравствуйте. Благодарим за регистрацию на нашем сайте.<br/><br/>'.
            'Ваши реквизиты для входа на сайт:<br/>'.
            '<b>Логин</b>: '.$data['name'].
            '<br/><b>Пароль</b>: '.$password.
            '<br/>Вы можете сменить пароль в настройках вашего профиля';
        $message_text = 'Здравствуйте. Благодарим за регистрацию на нашем сайте.\n\n'.
            'Ваши реквизиты для входа на сайт:\n'.
            'Логин: '.$data['name'].
            '\nПароль: '.$password.
            '\nВы можете сменить пароль в настройках вашего профиля';

        $mailer->setTo($data['email']);
        $mailer->setSubject($subject);
        $mailer->setContentHtml($message_html);
        $mailer->setContentText($message_text);

        if (!$mailer->sendMail())
            $this->Log->write('added user by using ulogin but mail not sended', 'user id(' . $data['id'] . ')');
        
        $_SESSION['user'] = $data;
        setcookie('autologin', 'yes', time() + 3600 * 24 * Config::read('cookie_time'), '/');
        setcookie('userid', $data['id'], time() + 3600 * 24 * Config::read('cookie_time'), '/');
        setcookie('password', $data['passw'], time() + 3600 * 24 * Config::read('cookie_time'), '/');

        if (Config::read('__secure__.system_log')) {
            $this->Log->write('added user by using ulogin', 'user id(' . $data['id'] . ')');
        }

        header('Location: '.(used_https() ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].'/users/info/'.$data['id']);
    }


    // функция возвращает html страницы ошибки
    public function error($markets, $nickname, $token) {
        $markets['{{ nickname }}'] = $nickname;
        $markets['{{ token }}'] = $token;
        $template = file_get_contents(dirname(__FILE__).'/template/error.html');
        return str_replace(array_keys($markets), $markets, $template);
    }
}
?>