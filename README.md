## Плагин uLogin для Atom-M CMS

uLogin — это инструмент, который позволяет пользователям получить единый доступ к различным Интернет-сервисам без необходимости повторной регистрации, а владельцам сайтов — получить дополнительный приток клиентов из социальных сетей и популярных порталов (Google, Яндекс, Mail.ru, ВКонтакте, Facebook и др.)

##Возможности плагина:

 - Автоматическая регистрация на сайте используя данные из выбранного сервиса
 - Если ник занят или не соответствует требованиям то появляется промежуточная страница со сменой ника
 - После регистрации редирект на свой профиль
 - После авторизации редирект на главную
 - После регистрации приходит почта с приветствием и паролем
 - В админке в настройке плагина можно указать uLogin ID или выбрать параметры виджета, такие как его тип и список поддерживаемых сервисов авторизации

## Установка:
После установки плагина из каталога плагинов (Админка->Плагины->Менеджер плагинов) выведите список сервисов авторизации в удобном для вас месте шаблона с помощью метки {{ ulogin }}